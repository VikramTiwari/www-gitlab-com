---
layout: markdown_page
title: "Content Marketing"
---

Welcome to the Content Marketing Handbook

[Up one level to the Product Marketing Handbook](/handbook/marketing/product-marketing/)    

## On this page
* [Introduction](#intro)
* [2016 activities](#2016)
* [Webcast Overview](#webcast)
* [Scheduling webcasts](#scheduling)
* [Webcast followup](#followup)
* [Blog Overview](#blog)
* [Get published on the blog](#getpublished)  
* [Blog ideas](#ideas)  


## Introduction<a name="intro"></a>

The philosophy for Content Marketing at GitLab is to help bring insider
knowledge and implicit practice to the hands of developers getting acquainted
with GitLab, and decision makers considering GitLab.

We want to meet each user where they are now, and help them be the most
efficient they can be with our tools.
We want to help their teams realize their creative and collaborative goals.

We also want to address the whole organization, the coal-face of development
or in decision making roles. GitLab is not only used for code development and
review. Those same tools are used by team members to track progress, create
documentation and collaborate on projects. When we show how we work "inside
GitLab" we also model how to use our software.

## 2016 activities<a name="2016"></a>

- Publish an active [blog](blog/) with useful content relevant to GitLab users.
- Host [webcasts](webcasts/)
which welcome newcomers, celebrate with existing users, and provide access to expertise.
- Publish a twice-monthly email newsletter you can sign up to on [our contact page](https://about.gitlab.com/contact/).

## What is the webcast program at GitLab?<a name="webcast"></a>

-   The webcast program consists of regular online events for GitLab users and community members.
-   The webcasts are recorded and distributed to GitLab users, and can be referred to in the Resource Library as part of GitLab University

- [How to Schedule a webcast](#schedule)
- [Technical requirements to view a webcast](#view)


### Monthly Program

This program is in development. For the first month, January, we'll start with one webcast, then add another and build up to the full schedule.

### Welcome to GitLab - 20 mins (coming soon!)

-   Live demo run.
-   Similar base demo script each month.
    -   How to use GitLab - essential demo.
    -   This month’s Q+A (commonly asked questions, recent questions from twitter)
    -   GitLab’s products and services.
    -   How to find stuff in the GitLab; getting help, direction, participation.
    -   Community welcome mat: How to meet other GitLab users what events we’ll be at, how about you?
-   Aimed at developers or decision makers who have signed up in the last 30 days, inviting them into the community.

### Release Party - 30 - 40 mins

-   Monthly Thursday following a release.
-   Present highlights from the new features.
-   Refer to any resources, docs, screencasts, etc.
-   Guest speakers from the dev team about the new features.
-   Highlight contributors and the MVP for that month.
-   New contributors welcomed.
-   Q+A from audience.

### GitLab Tutorials - 40 mins - 1 hour

-   Live presentation, demo or discussion on monthly in-depth learning theme.
-   Preceded by 3 weeks of blog posts, screencasts, tutorial and an invitation which leads to the online event.
-   Live event includes: Guest speaker / interview / presentation / demo as appropriate to topic.
-   Q+A from audience. Survey to GitLab users on the topic if appropriate.
-   After event: Blog post of findings from the Q+A, results of survey.
-   Roll-up content into an downloadable ebook, course or other way to make the content more easily accessed and reviewed.

## Scheduling webcasts<a name="schedule"></a>

- Webcasts are on Thursdays, 17:00 UTC (9am PST, 6pm CET)
- Panelists should arrive 15 mins before the webcast for a final sound check
- Panelists should participate in a rehearsal before the webcast

#### Webcast configuration checklist

- Set up webinar in Gotowebinar by cloning the most recent webcast.
- Add a welcome message to attendees
- Upload any handouts (up to 5)
- Upload images, branding and speaker photos (must be jpg or gif and the exact dimensions)

#### Create a calendar event

GoToWebinar doesn't send out a Google calendar friendly invite

- Create an invitation for speakers in Google calendar
- Include their actual URL from their invite.
- Do the same for the rehearsal invitation.


#### Create the program in Marketo

- Clone the last campaign
- Connect the webcast campaign to Gotowebinar. Webcast > Event Settings > Event partner
- Correct version number and invitation date and details for assets:
    -   The landing page
    -   The confirmation email
- Update the hero form on the landing page to the correct form.
- Final check: If the lightbulb is not "on" (yellow), then it's not doing anything.
Check the smart list and flow first. To activate: Click the "registered" smart campaign -> "Schedule" tab -> "Activate" button

#### Promote

- Publish to Facebook
- Schedule tweets
- Create a blog post
- Add to next newsletter

#### As the event starts

- Promote [the appropriate attendees to panelist](https://support.citrixonline.com/en_US/webinar/knowledge_articles/000027765)
- Conduct a sound check and sharing check for anyone who will present.
- Organizers and Panelists are listed in the "Staff" tab and they can mute and unmute attendees, and see questions sent to the Panelists, etc.

## After the webcast<a name="followup"></a>

- Process the recording as .mov
- Upload to YouTube and slides to Speakerdeck
- Blog post to share the recording and slides


### <a name="view"></a>Viewing webcasts

- [Citrix Online system requirements](https://support.citrixonline.com/webinar/all_files/G2W010003)
- Using GoToWebinar Instant Join, Linux/Unbuntu users can view in a web browser.
- Linux users should use Chromium to view the browser.


## Blog Overview<a name="blog"></a>

The [blog](/blog) is our main publishing outlet.

- We aim to publish content multiple times a week, with a reliable publishing
schedule
- We also want to bring in voices from all
throughout the company, as well as from GitLab users and our customers.
- Content should communicate the benefits of GitLab's unique innovations and tools (CI)

### Formats

- Short form articles
- Long form articles
- Release announcements
- Feature highlights
- Tutorials
- Inside GitLab

### Product-specific topics

- Tutorials on using GitLab, GitLab CI, etc.
- Feature highlights bring attention to specific features at GitLab.

### User Stories

* Contributor stories 'why I contribute to GitLab'
* Use case stories 'how we use GitLab'
* Boss stories 'how GitLab enabled innersourcing'
* Inception stories 'how GitLab uses GitLab'
* Adoption stories 'how we switched from SVN to GitLab'
* Customer stories 'why we choose GitLab'

## Getting published on the blog<a name="getpublished"></a>

Anyone from GitLab, Inc or within the community can propose an topic on the
GitLab [Blog Post Issue Tracker][blog-tracker].

We invite guest posts and also offer compensation through the [Community Writers](https://about.gitlab.com/community/writers) program.

1. Submit an issue on the [Blog post issue tracker][blog-tracker].
2. You'll get feedback on your proposal and outline.
3. Write your draft as a WIP MR (work in progress merge request) in the [GitLab website project][gitlabwww].
4. You'll get reviewed and feedback from our editors.

### About the Blog Post Issue Tracker

- Anything not assigned to a person is in the [backlog](https://dev.gitlab.org/gitlab/blog-posts/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&author_id=&assignee_id=0&milestone_title=&label_name=)
- Anything that is assigned to a person in 'in progress'
- Anything that has a WIP MR is ready for review

### Blog post publishing checklist

Before you write, make sure you're on a new branch cloned from master.
Check these before you publish:

- First instance of GitLab should be linked to [GitLab](http://about.gitlab.com)
- Follow the [Blog post style guide](https://gitlab.com/gitlab-com/blog-posts/blob/master/STYLEGUIDE.md)
- Check all links.
- Check the date on the file name.
- Check the date in the post.
- Check the image is crunched down. Use [tinypng](http://tinypng.com).
- Check the blog appears good locally.
- When you have double checked, you can merge!

It takes about 5 mins for the blog post to appear as published.

After the blog post is published we should tweet immediately from the GitLab
Twitter account, and schedule follow up tweets and LinkedIn and Facebook.


## Blog ideas<a name="ideas"></a>

* The content doesn't have to be about GitLab, it can also be other content aimed at developers, Hacker News or team leads.
* When submitting to Hacker News please add a ? to the url and do not announce it anywhere to prevent setting off the voting ring detector.
* You need to have high quality and high volume, great times are in the [Priceonomics content marketing handbook](http://priceonomics.com/the-content-marketing-handbook/).
* What worked for Apigee was the 'collaboration in the 21st century' theme
* Videos with good screencasts, great subtitles, and narratives are expensive but popular, and hard to copy (what does happens to written content), Realm.io does a lot of good videos, for example [about Swift](https://realm.io/news/top-5-swift-videos-of-2014/)
* A reading club such as [a NoSQL summer](http://nosqlsummer.org/)
* Milk [GitLab Flow](http://doc.gitlab.com/ee/workflow/gitlab_flow.html) for more blog posts and videos
* CI for mobile is painful (Gradle files for Android, loads of assets such as Xcode binaries) and the best current option is customizing Jenkins, mobile is a small circle that moves fast, collaborate with leading projects such as [CocoaPods](https://cocoapods.org/) for iOS (Sid can contract [Eloy Durán](https://twitter.com/alloy) and [Gradle](https://gradle.org/) for Android to create a great CI experience and blog about it
* Can accelerate the above with free CI runners
* Offer $100 per blog post and use a public issue tracker to gather idea's and tag them as acceptable.
* Encourage guest posts on our blog

[blog-tracker]: https://gitlab.com/gitlab-com/blog-posts/issues
[gitlabwww]: https://gitlab.com/gitlab-com/www-gitlab-com
